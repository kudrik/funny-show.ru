function ajaxFormRequest(form, resultId, returnType) {

  const data = form.serialize();
  //block all fields
  const children = form.find('input, textarea, button');
  children.each(function( index, value ) {
    $(value).prop('disabled', true);
  });

  return $.ajax({
    url: form.attr('action'), //Адрес подгружаемой страницы
    type: form.attr('method'), //Тип запроса
    dataType: returnType, //Тип данных
    data: data,
    success: function (response) {
      if (returnType !== 'script') {
        //Если все нормально
        $("#" + resultId).html(response);
      }
    },
    error: function (response) {
      alert('Ошибка при отправке данных');
    }
  });
}

$(document).ready(function () {

  //make smooth scroll for all links with anchors
  $('a').click(function (event) {

    const href = event.target.href;

    if (href.indexOf('#') > -1) {
      const targetId = href.split('#')[1];

      if (targetId) {
        $([document.documentElement, document.body]).animate({
          scrollTop: $('#' + targetId).offset().top
        }, 1000);
        event.preventDefault();
      }
    }
  });

  //play/stop for header video background
  $('#bgVideoControl').click(function (event) {
    const controlObj = $(event.target);
    const videoObj = $('#bgVideo');
    if (controlObj.hasClass('e-control-stop')) {
      controlObj.removeClass('e-control-stop').addClass('e-control-play');
      videoObj.fadeOut('slow');
    } else {
      controlObj.removeClass('e-control-play').addClass('e-control-stop');
      videoObj.fadeIn('fast');
    }
    event.preventDefault();
  });
});