$(document).ready(function(){

    $('.easyadmin-list-editable input').change(function () {

        const data = {
            fieldname:  this.dataset.propertyname,
            value: this.value
        };

        const input = this;
        input.disabled = true;

        let toggleRequest = $.ajax({ type: 'POST', url: this.dataset.path+'&id='+this.dataset.id, data: data });

        toggleRequest.done(function(result) { input.disabled = false; });
        toggleRequest.fail(function() {
            alert ('error');
        });

    });

});