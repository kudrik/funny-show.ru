<?php

declare(strict_types=1);

namespace App\Command;

use App\Application\Search\SearchService;
use App\Entity\Catalog;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SearchReindexCommand extends Command
{
    protected static $defaultName = 'search:reindex';

    private ManagerRegistry $managerRegistry;

    private SearchService $searchService;

    public function __construct(ManagerRegistry $managerRegistry, SearchService $searchService)
    {
        parent::__construct();
        $this->managerRegistry = $managerRegistry;
        $this->searchService = $searchService;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Catalog[] $catalog */
        $catalog = $this->managerRegistry->getRepository(Catalog::class)->findAll();

        foreach ($catalog as $item) {
            $this->searchService->update($item);
        }

        return 0;
    }
}