<?php

declare(strict_types=1);

namespace App\Command;


use App\Application\Search\SearchService;
use App\Entity\Catalog;
use App\Entity\CatalogItem;
use App\Entity\Page;
use App\Entity\Tag;
use App\Entity\TagCategory;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Pagerfanta\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class TestCommand extends Command
{
    protected static $defaultName = 'app:test';

    private ManagerRegistry $managerRegistry;

    private SerializerInterface $serializer;

    private SearchService $searchService;

    private ParameterBagInterface $parameterBag;

    public function __construct(ManagerRegistry $managerRegistry, SerializerInterface $serializer, SearchService $searchService, ParameterBagInterface $parameterBag)
    {
        parent::__construct();
        $this->managerRegistry = $managerRegistry;
        $this->serializer = $serializer;
        $this->searchService = $searchService;
        $this->parameterBag = $parameterBag;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Catalog[] $catalog */
        $catalog = $this->managerRegistry->getRepository(Catalog::class)->findBy(['page' => 2]);

        $tagRepository = $this->managerRegistry->getRepository(Tag::class);

        $tagCategory = $this->managerRegistry->getRepository(TagCategory::class)->find(18);


        foreach ($tagRepository->findAll() as $tag) {
            if ($tag->getCatalog()->count() === 0) {
                print_R($tag->getName());
                echo PHP_EOL;

                $this->managerRegistry->getManager()->remove($tag);
                $this->managerRegistry->getManager()->flush();
            }
        }

        return 0;


        $oldTags = [];

        foreach ($catalog as $item) {

            $oldTag = $item->getTags()->filter(fn(Tag $tag) => $tag->getTagCategory()->getId() === $tagCategory->getId())->first();

            if (!$oldTag) {
                continue;
            }

            if (!preg_match('/(\d+)м\//ui', $oldTag->getName(), $match)) {
                continue;
            }

            $newValue = $match[1]*$item->weight/50;

            if (!($tag = $tagRepository->findOneBy(['name' => $newValue, 'tagCategory' => $tagCategory]))) {
                $tag = new Tag();
                $tag->setName((string) $newValue);
                $tag->setTagCategory($tagCategory);
                $this->managerRegistry->getManager()->persist($tag);
                $this->managerRegistry->getManager()->flush();
            }

            $item->removeTag($oldTag);
            $item->addTag($tag);

            print_R($match);

        }
        $this->managerRegistry->getManager()->flush();

        return 0;

        /** @var Catalog[] $catalog */
        $catalog = $this->managerRegistry->getRepository(Catalog::class)->findAll();

        foreach ($catalog as $item) {

            if (!$item->tmpImageFileName) {
                continue;
            }

            $uploadedFile = null;

            try {
                $uploadedFile = new UploadedFile($this->parameterBag->get('images.base_dir').'catalog/'.$item->tmpImageFileName, (string) $item->getId());
            } catch (\Exception $exception) {
                echo $exception->getMessage();
                echo PHP_EOL;
            }

            if (!$uploadedFile) {
                continue;
            }

            echo PHP_EOL.PHP_EOL;
            echo '--==='.$item->getId().'===---'.PHP_EOL;

            $continue = false;

            /** @var CatalogItem $catItem */
            foreach ($item->getItems() as $catItem) {
                if ($catItem->getImageFileName()) {

                    $catItemImage = $this->parameterBag->get('images.base_dir').$catItem->getImageBaseName();

                    if (@md5_file($catItemImage) === @md5_file($uploadedFile->getRealPath())) {

                        echo $catItemImage;
                        echo ' = ';
                        echo $uploadedFile->getRealPath();

                        echo PHP_EOL;

                        $continue = true;

                        break;
                    }
                }
            }

            if ($continue) {
                continue;
            }

            /** @var CatalogItem $catalogItem */
            $catalogItem = $item->getItems()->first();

            if (!$catalogItem || $catalogItem->getImageFileName()) {

                $catalogItem = new CatalogItem();
                $catalogItem->amount = 1;
                $catalogItem->partNumber = '1000001';
                $item->addItem($catalogItem);
                $this->managerRegistry->getManager()->persist($item);
            }

            $catalogItem->setImageFile($uploadedFile);

            echo $catalogItem->partNumber;
            echo PHP_EOL;

        }

        $this->managerRegistry->getManager()->flush();

        return 0;


        $tagCategory = $this->managerRegistry->getRepository(TagCategory::class)->find(3);

        $tagRepository = $this->managerRegistry->getRepository(Tag::class);

        /** @var Catalog[] $catalog */
        $catalog = $this->managerRegistry->getRepository(Catalog::class)->findBy(['page'=> 2]);

        foreach ($catalog as $item) {

            $oldName = $item->name;

            $anonse = '';

            if (preg_match_all('/\d+ ?%\D*/', $item->name, $matches)) {

                foreach ($matches[0] as $sostav) {
                    $item->name = str_replace($sostav,  '', $item->name);
                    $anonse.=$sostav;
                }

                $anonse = trim($anonse);
            }

            if ($anonse) {
                $anonse.=', ';
            }

            if ($item->weight > 0) {

                if ($oldTag = $item->getTags()->filter(fn(Tag $tag) => $tag->getTagCategory()->getId() === 18)->first()) {
                    $length = 50 / $item->weight * $oldTag->getName();

                    $anonse.= round($length) .'м/50г';
                }
            }

            $item->name = trim($item->name);
            $anonse = trim($anonse);
            $anonse = str_replace('  ', ' ', $anonse);
            $anonse = str_replace(' %','% ', $anonse);
            $anonse = str_replace('%','% ', $anonse);
            $anonse = str_replace('  ', ' ', $anonse);
            $anonse = trim($anonse);
            $item->anonse = $anonse;

            print_R([$oldName, $item->name,  $item->anonse ]);

        }

        $this->managerRegistry->getManager()->flush();

        return 0;

        foreach ($catalog as $item) {

            $value = null;

            $oldTag = $item->getTags()->filter(fn(Tag $tag) => $tag->getTagCategory()->getId() === 18 )->first();

            $length = 50/$item->weight*$oldTag->getName();

            $lengthRounded = round($length, -2);
            if ($lengthRounded < $length) {
                $lengthRounded+=100;
            }


            $newValue = ($lengthRounded-100).'-'.$lengthRounded.'м';
            if ($lengthRounded < 101) {
                $newValue = '< 100м';
            }




            if (!($tag = $tagRepository->findOneBy(['tagCategory' => $tagCategory, 'name' => $newValue]))) {
                $tag = new Tag();
                $tag->setTagCategory($tagCategory);
                $tag->setName($newValue);
                $this->managerRegistry->getManager()->persist($tag);
                $this->managerRegistry->getManager()->flush();
            }

            $item->addTag($tag);

            echo $length . ' - '. $newValue;
            echo PHP_EOL;

            //echo $item->name;
        }

        $this->managerRegistry->getManager()->flush();


        return 0;

        $imageSourcePath = 'old/upload/';

        $page = $this->managerRegistry->getRepository(Page::class)->find(8);

        $tagCategoryRepository = $this->managerRegistry->getRepository(TagCategory::class);

        $tagCategory = $tagCategoryRepository->find(1);

        $tagCategory->addPage($page);

        $this->managerRegistry->getManager()->flush();


        echo  $tagCategory->getName();


        die;



        $tagRepository = $this->managerRegistry->getRepository(Tag::class);

        $catalogRepository = $this->managerRegistry->getRepository(Catalog::class);

        /** @var Connection $connection */
        $connection = $this->managerRegistry->getConnection(null);

        $articlePropertyId = 15;

        /** @var Catalog $catalog */
        foreach ($catalogRepository->findAll() as $catalog) {

            echo $catalog->getId().PHP_EOL;

            $uniq_items = [];

            //get properties
            $filesDownload = false;
            $q_where = 'FROM b_iblock_element_property WHERE IBLOCK_ELEMENT_ID = '.$catalog->importId.' AND IBLOCK_PROPERTY_ID = '.$articlePropertyId;
            $q = 'SELECT COUNT(*) AS total '.$q_where;
            $stmt2 = $connection->prepare($q);
            $stmt2->execute();
            $row2 = $stmt2->fetch();
            if ($row2['total'] > 1) {
                $filesDownload = true;
            }

            $q = 'SELECT * '.$q_where;
            $stmt2 = $connection->prepare($q);
            $stmt2->execute();
            foreach ($stmt2->fetchAll() as $row2) {

                $catalogItem = new CatalogItem();
                $catalogItem->catalog = $catalog;
                $catalogItem->partNumber = $row2['DESCRIPTION'];

                if (preg_match('/\(([0-9]+).*?шт.*?\)/miu', $row2['DESCRIPTION'], $match)) {
                    $catalogItem->partNumber = str_replace($match[0], '', $row2['DESCRIPTION']);
                    $catalogItem->amount = (int) $match[1];
                }

                //get main picture
                if ($filesDownload) {
                    $q = 'SELECT * FROM b_file WHERE ID = '. $row2['VALUE'].' LIMIT 1';
                    $stmt3 = $connection->prepare($q);
                    $stmt3->execute();
                    $fileRow = $stmt3->fetch();

                    $sourceFile = $imageSourcePath.$fileRow['SUBDIR'].'/'.$fileRow['FILE_NAME'];
                    if ($fileRow['ID'] && getimagesize($sourceFile)) {
                        $file = new UploadedFile($sourceFile, $fileRow['FILE_NAME']);
                        $catalogItem->setImageFile($file);
                    }
                }


                if (!isset($uniq_items[$catalogItem->partNumber])) {
                    $catalog->addItem($catalogItem);
                }

                $uniq_items[$catalogItem->partNumber] = 1;

            }

            $this->managerRegistry->getManager()->flush();
            $this->logs['done']++;
        }

        dump($this->logs);


        /*

        $result = $catalogRepository->findAll();


        foreach ($result as $catalog) {

            //get properties
            $q2 = 'SELECT * FROM b_iblock_element_property WHERE IBLOCK_ELEMENT_ID = '.$catalog->importId;
            $stmt2 = $connection->prepare($q2);
            $stmt2->execute();
            foreach ($stmt2->fetchAll() as $row2) {

                //price
                if ($row2['IBLOCK_PROPERTY_ID'] == 8) {
                    $catalog->country = $row2['VALUE'];
                }

                if ($row2['IBLOCK_PROPERTY_ID'] == 5) {
                    $catalog->weight = (int) $row2['VALUE'];
                }

                if ($row2['IBLOCK_PROPERTY_ID'] == 6 && $catalog->getPage()->getId() === 2) {

                    $dlina = (int) trim($row2['VALUE']);

                    if (!($tag = $tagRepository->findOneBy(['name' => $dlina]))) {
                        $tag = new Tag();
                        $tag->setName((string) $dlina);
                        $tag->setTagCategory($tagCategory);
                        $this->managerRegistry->getManager()->persist($tag);
                        $this->managerRegistry->getManager()->flush();
                    }

                    $catalog->addTag($tag);
                }
            }

            //echo $catalog->getId();
        }

        $this->managerRegistry->getManager()->flush();

        ///-=================================================

            /*
            $result = $catalogRepository->findBy(['page' => 2]);


            foreach ($result as $catalog) {

                if (preg_match_all('/\d+\s?%\s?([\p{Cyrillic}| ]+)/mu', $catalog->name, $match)) {

                    foreach ($match[1] as $value) {
                        $value = trim($value);
                        $value = mb_strtolower($value);

                        if (!($tag = $tagRepository->findOneBy(['name' => $value, 'tagCategory' => $tagCategory]))) {
                            $tag = new Tag();
                            $tag->setTagCategory($tagCategory);
                            $tag->setName($value);

                            $this->managerRegistry->getManager()->persist($tag);
                            $this->managerRegistry->getManager()->flush();
                        }

                        $catalog->addTag($tag);

                        $this->managerRegistry->getManager()->flush();
                    }




                    print_R($match[1]);
                    echo PHP_EOL;
                }



            }
            */

         /*
        $result = $qb->select('c.id,c.name')
            ->from('kt_catalog', 'c')
            ->leftJoin('c', 'kt_catalog_tag', 'rct', 'rct.catalog_id = c.id')
            ->leftJoin('c', 'kt_tag', 't', 't.id = rct.tag_id AND t.tag_category_id = ?')
            ->where('t.id IS NULL AND c.name LIKE "%(%)%"')
            ->setParameter(0, $tagCategory->getId())
            ->execute();

        foreach ($result as $item) {


            preg_match('/\((.*?)\)/', $item['name'], $match);

            $tagValue = trim($match[1]);

            if (!($tag = $tagRepository->findOneBy(['name' => $tagValue, 'tagCategory' => $tagCategory]))) {
                $tag = new Tag();
                $tag->setTagCategory($tagCategory);
                $tag->setName($tagValue);

                $this->managerRegistry->getManager()->persist($tag);
                $this->managerRegistry->getManager()->flush();
            }

            $catalog = $catalogRepository->find($item['id']);

            $catalog->addTag($tag);

            $this->managerRegistry->getManager()->flush();

            print_R($item);

        }
        */




        return 0;
    }


}