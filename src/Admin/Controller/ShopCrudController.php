<?php

namespace App\Admin\Controller;

use App\Entity\Shop;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;

class ShopCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Shop::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setSearchFields(['id', 'position', 'name', 'price', 'amount', 'description', 'vendor', 'model', 'imageFileName'])
            ->setDefaultSort(['page.lft' => 'ASC', 'position' => 'ASC'])
            ->setPaginatorPageSize(50);
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->disable('show');
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(EntityFilter::new('page'))
            ->add('name');
    }

    public function configureFields(string $pageName): iterable
    {
        $page = AssociationField::new('page');
        $name = TextField::new('name');
        $price = IntegerField::new('price');
        $amount = IntegerField::new('amount');
        $vendor = TextField::new('vendor');
        $model = TextField::new('model');
        $description = TextareaField::new('description');
        $published = BooleanField::new('published');
        $position = IntegerField::new('position');
        $imageFile = ImageField::new('imageFile');
        $id = IntegerField::new('id', 'ID');
        $image = ImageField::new('image'); //->setTemplatePath('admin/field_imageable.html.twig');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $page, $name, $image, $price, $amount, $published];
        }

        return [$page, $name, $price, $amount, $vendor, $model, $description, $published, $imageFile, $position];
    }
}
