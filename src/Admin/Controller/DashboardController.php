<?php

namespace App\Admin\Controller;

use App\Entity\Catalog;
use App\Entity\Country;
use App\Entity\Menu;
use App\Entity\Order;
use App\Entity\Page;
use App\Entity\Photo;
use App\Entity\Product;
use App\Entity\Shop;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

        return $this->redirect($routeBuilder->setController(PageCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Funny Show');
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linkToDashboard('Dashboard', 'fa fa-home'),

            MenuItem::section('Basic'),
            MenuItem::linkToCrud('Pages', 'fa fa-folder', Page::class),
            MenuItem::linkToCrud('Menu', 'fa fa-folder', Menu::class),

            MenuItem::section('Features'),
            MenuItem::linkToCrud('Catalog', 'fa fa-folder', Catalog::class),
            MenuItem::linkToCrud('Shop', 'fa fa-folder', Shop::class),
            MenuItem::linkToCrud('Photo', 'fa fa-folder', Photo::class),
            MenuItem::linkToCrud('Order', 'fa fa-folder', Order::class),

            MenuItem::section('Users'),
            MenuItem::linkToCrud('Users', 'fa fa-folder', User::class),
        ];
    }
}
