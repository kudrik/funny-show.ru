<?php

namespace App\Admin\Controller;

use App\Entity\Catalog;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CatalogCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Catalog::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setSearchFields(['id', 'name', 'position', 'anonse', 'description', 'price', 'imageFileName'])
            ->setPaginatorPageSize(50);
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->disable('show');
    }

    public function configureFields(string $pageName): iterable
    {
        $name = TextField::new('name');
        $description = TextareaField::new('description');
        $imageFile = Field::new('imageFile', 'Титульное изображение');
        $price = IntegerField::new('price');
        $published = Field::new('published');
        $showOnMainPage = Field::new('showOnMainPage');
        $position = IntegerField::new('position');
        $items = AssociationField::new('items');
        $id = IntegerField::new('id', 'ID');
        $createdAt = DateTimeField::new('createdAt');
        $updatedAt = DateTimeField::new('updatedAt');
        $anonse = TextField::new('anonse');
        $imageFileName = TextField::new('imageFileName');
        $imageUpdatedAt = DateTimeField::new('imageUpdatedAt');
        $image = ImageField::new('Image')->setTemplatePath('admin/field_imageable.html.twig');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $name, $image, $price, $showOnMainPage, $published];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $createdAt, $updatedAt, $published, $showOnMainPage, $name, $position, $anonse, $description, $price, $imageFileName, $imageUpdatedAt, $items];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$name, $description, $imageFile, $price, $published, $showOnMainPage, $position, $items];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$name, $description, $imageFile, $price, $published, $showOnMainPage, $position, $items];
        }
    }
}
