<?php

namespace App\Admin\Controller;

use App\Entity\Order;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class OrderCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Order::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setSearchFields(['id', 'theme', 'description', 'client.name', 'client.phone', 'requestInfo.ip', 'requestInfo.referrer'])
            ->setPaginatorPageSize(50);
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->disable('new');
    }

    public function configureFields(string $pageName): iterable
    {
        $client = Field::new('client');
        $done = Field::new('done');
        $theme = TextField::new('theme');
        $description = TextareaField::new('description');
        $id = IntegerField::new('id', 'ID');
        $createdAt = DateTimeField::new('createdAt');
        $clientName = TextField::new('client.name');
        $clientPhone = TextField::new('client.phone');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $createdAt, $clientPhone, $theme, $description, $done];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $createdAt, $clientName, $clientPhone, $done, $theme, $description];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$client, $done, $theme, $description];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$client, $done, $theme, $description];
        }
    }
}
