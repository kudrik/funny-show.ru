<?php

namespace App\Admin\Controller;

use App\Entity\Photo;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;

class PhotoCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Photo::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Photo')
            ->setEntityLabelInPlural('Photo')
            ->setSearchFields(['id', 'name', 'position', 'imageFileName'])
            ->setPaginatorPageSize(50);
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->disable('show');
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(EntityFilter::new('page'))
            ->add('name');
    }

    public function configureFields(string $pageName): iterable
    {
        $page = AssociationField::new('page');
        $name = TextField::new('name');
        $imageFile = Field::new('imageFile', 'Титульное изображение');
        $position = IntegerField::new('position');
        $id = IntegerField::new('id', 'ID');
        $imageFileName = TextField::new('imageFileName');
        $imageUpdatedAt = DateTimeField::new('imageUpdatedAt');
        $image = ImageField::new('Image')->setTemplatePath('admin/field_imageable.html.twig');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $page, $name, $image];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $name, $position, $imageFileName, $imageUpdatedAt, $page];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$page, $name, $imageFile, $position];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$page, $name, $imageFile, $position];
        }
    }
}
