<?php

namespace App\Admin\Controller;

use App\Entity\Page;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Page::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setSearchFields(['id', 'name', 'url', 'anonse', 'description', 'meta.title', 'meta.description', 'meta.keywords'])
            ->setDefaultSort(['lft' => 'ASC'])
            ->setPaginatorPageSize(50);
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->disable('show');
    }

    public function configureFields(string $pageName): iterable
    {
        $basicPanel = FormField::addPanel('Basic information');
        $parent = AssociationField::new('parent');
        $name = TextField::new('name');
        $url = TextField::new('url')->setRequired(false);
        $seoPanel = FormField::addPanel('SEO');
        $metaTitle = TextField::new('meta.title');
        $metaDescription = TextField::new('meta.description');
        $metaKeywords = TextField::new('meta.keywords');
        $panel3 = FormField::addPanel('');
        $anonse = TextareaField::new('anonse');
        $imageFile = ImageField::new('imageFile', 'Image');
        $description = TextareaField::new('description');
        $position = IntegerField::new('position');
        $id = IntegerField::new('id', 'ID');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $name, $url, $anonse];
        }

        return [$basicPanel, $parent, $name, $url, $seoPanel, $metaTitle, $metaDescription, $metaKeywords, $panel3, $anonse, $imageFile, $description, $position];
    }
}
