<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\Page;
use App\Repository\PageRepository;

class MenuConfigPass extends \EasyCorp\Bundle\EasyAdminBundle\Configuration\MenuConfigPass
{
    private PageRepository $pageRepository;

    public function __construct(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    public function process(array $backendConfig): array
    {
        $backendConfig = parent::process($backendConfig);

        foreach ($backendConfig['design']['menu'] as $i => $itemConfig) {

            if ($itemConfig['entity'] === 'Shop') {
                /** @var Page $page */
                foreach ($this->pageRepository->findShopRubricator() as $page) {
                    $this->addSubMenu(
                        $backendConfig,
                        $i,
                        $page->getName(),
                        [
                            'entity' => $itemConfig['entity'],
                            'filters' => [
                                'page' => [
                                    'comparison' => '=',
                                    'value' => $page->getId(),
                                ],
                            ],
                        ],
                        $page->lvl
                    );
                }
            }
        }


        return $backendConfig;
    }

    private function addSubMenu(array &$backendConfig, int $menuIndex, string $label, array $params, int $level): void
    {
        if (!isset($backendConfig['design']['menu'][$menuIndex]['children'])) {
            $backendConfig['design']['menu'][$menuIndex]['children'] = [];
        }

        $count = count($backendConfig['design']['menu'][$menuIndex]['children']);

        $backendConfig['design']['menu'][$menuIndex]['children'][] = [
            'type' => 'route',
            'label' => $this->buildNameWithLevel($label, $level),
            'permission' => [],
            'submenu_index' => $count,
            'menu_index' => $menuIndex,
            'route' => 'easyadmin',
            'params' => $params,
            'icon' => null,
        ];
    }

    private function buildNameWithLevel(string $name, int $level): string
    {
        return str_repeat('-', $level - 2).$name;
    }
}