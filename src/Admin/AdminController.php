<?php

declare(strict_types=1);

namespace App\Admin;

use App\Application\EasyAdmin\Controller\EasyAdminController;
use App\Entity\Page;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AdminController extends EasyAdminController
{
    public function createPhotoEntityFormBuilder($entity, $view)
    {
        $formBuilder = parent::createEntityFormBuilder($entity, $view);

        /** @var Page $photoPage */
        $photoPage = $this->getDoctrine()->getRepository(Page::class)->findPhoto();

        $formBuilder->add('page', EntityType::class, ['class' => Page::class, 'choices' => $photoPage->children]);

        return $formBuilder;
    }
}