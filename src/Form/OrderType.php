<?php

declare(strict_types=1);

namespace App\Form;

use App\Application\Form\AbstractType;
use App\Entity\Order;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Order::class,
                'attr' => ['class' => 'b-form', 'id' => 'orderform'],
                'label_attr' => ['class' => 'formTitle'],
                //'label' => you can change label value in App\Service\OrderFormFactory
            ]
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        /** @var Order $order */
        $order = $builder->getData();
        if ($order->getCatalog()) {
            $builder->add('theme', TextType::class, ['attr' => ['readonly' => true ]]);
        } else {
            $builder->add('theme',HiddenType::class);
        }

        $builder
            ->add('client', ClientType::class)
            ->add(
                'description',
                TextareaType::class,
                ['required' => false, 'attr' => ['placeholder' => 'Ваш комментарий']]
            )
            ->add('submit', SubmitType::class, ['label' => 'Отправить', 'attr' => ['class' => 'b-but']]);
    }
}