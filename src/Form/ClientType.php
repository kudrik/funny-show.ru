<?php

declare(strict_types=1);

namespace App\Form;

use App\Application\Form\AbstractType;
use App\Entity\Client;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Client::class,
            'attr' => ['class' => 'b-form']
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('name', TextType::class, ['attr' => ['placeholder' => 'Ваше имя *']])
            //->add('email', EmailType::class, ['attr' => ['placeholder' => 'Ваш e-mail *']])
            ->add('phone', TelType::class, ['attr' => ['placeholder' => '+7 XXX XXX XX XX *']]);
    }
}