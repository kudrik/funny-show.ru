<?php

declare(strict_types=1);

namespace App\Form;

use App\Application\Imageable\ImageableFileType;
use App\Entity\CatalogItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CatalogItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, static function(FormEvent $formEvent) {
            /** @var CatalogItem $catalogItem */
            $catalogItem = $formEvent->getData();

            $builder = $formEvent->getForm();
            
            $builder->add('imageFile', ImageableFileType::class, ['label' => 'Image', 'required' => false]);
        });
        //$builder->add('main', CheckboxType::class, ['required' => false, 'label' => 'Make it main']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CatalogItem::class,
            'label' => '-----'
        ]);
    }
}