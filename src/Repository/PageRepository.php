<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Page;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;

class PageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Page::class);
    }

    public function findShopRubricator(): iterable
    {
        /** @var Page $shopPage */
        $shopPage = $this->findOneBy(['parent' => null, 'url' => 'shop']);

        return $shopPage ? $this->findDescendants($shopPage) : [];
    }

    public function findDescendants(Page $page): iterable
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->gt('lft', $page->lft));
        $criteria->andWhere(Criteria::expr()->lt('rgt', $page->rgt));
        $criteria->orderBy(['lft' => 'ASC']);

        return $this->matching($criteria);
    }

    public function findAncestors(Page $page): iterable
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->lt('lft', $page->lft));
        $criteria->andWhere(Criteria::expr()->gt('rgt', $page->rgt));

        return $this->matching($criteria);
    }

    public function findCatalog(): ?Page
    {
        return $this->findOneBy(['parent' => null, 'url' => 'catalog']);
    }

    public function findPhoto(): ?Page
    {
        return $this->findOneBy(['parent' => null, 'url' => 'photos']);
    }
}