<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Tag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;

class TagRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tag::class);
    }

    public function findUsedBy(Criteria $criteria): array
    {
        $qb = $this->createQueryBuilder('t')
            ->select('t')
            ->innerJoin('t.catalog', 'c')
            ->addCriteria($criteria)
            ->groupBy('t.id')
            ->orderBy('t.name', 'ASC');

        return $qb->getQuery()->getResult();
    }
}