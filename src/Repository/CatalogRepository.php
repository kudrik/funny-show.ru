<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Catalog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CatalogRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Catalog::class);
    }


    public function findForMainPage(): array
    {
        return $this->findBy(['published' => true, 'showOnMainPage' => true], ['position' => 'asc']);
    }
}