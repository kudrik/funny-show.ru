<?php

declare(strict_types=1);

namespace App\Application\Sortable;

use Doctrine\Common\Collections\Criteria;

trait SortableTrait
{
    public int $position = 0;

    public function getMaxPositionCriteria(): Criteria
    {
        return new Criteria();
    }
}