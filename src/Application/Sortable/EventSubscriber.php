<?php

declare(strict_types=1);

namespace App\Application\Sortable;

use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class EventSubscriber implements \Doctrine\Common\EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $this->process($args);
    }

    private function supports(object $entity): bool
    {
        return in_array(SortableTrait::class, class_uses($entity));
    }

    protected function process(LifecycleEventArgs $args): void
    {
        /** @var SortableTrait $entity */
        $entity = $args->getObject();

        if ($this->supports($entity) && !($entity->position > 0)) {

            $highest_position = $args->getEntityManager()
                ->createQueryBuilder()
                ->select('MAX(e.position)')
                ->from(get_class($entity), 'e')
                ->addCriteria($entity->getMaxPositionCriteria())
                ->getQuery()
                ->getSingleScalarResult();

            $entity->position = ($highest_position + 10);
        }
    }
}