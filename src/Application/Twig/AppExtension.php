<?php

namespace App\Application\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('price', [$this, 'priceFilter']),
            new TwigFilter('tel', [$this, 'phoneRusFormater']),
        ];
    }

    public function priceFilter(?int $number): ?string
    {
        return number_format($number, 0,'.',' ');
    }

    public function phoneRusFormater(?string $phone, string $country = 'RU'): ?string
    {
        if ($phone) {
            if (strlen($phone) === 11 && ($country === 'RU' || $country === 'KZ')) {
                return '+'.$phone[0]
                    . ' (' .substr($phone, 1, 3).') '
                    . substr($phone, 4, 3) . '-'
                    . substr($phone, 7, 2) . '-'
                    . substr($phone, 9, 2);
            }

            return $phone;
        }

        return null;
    }
}