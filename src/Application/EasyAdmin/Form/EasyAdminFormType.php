<?php

declare(strict_types=1);

namespace App\Application\EasyAdmin\Form;

use App\Application\Form\PropertyPathMapper;
use EasyCorp\Bundle\EasyAdminBundle\Configuration\ConfigManager;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class EasyAdminFormType extends \EasyCorp\Bundle\EasyAdminBundle\Form\Type\EasyAdminFormType
{
    private PropertyPathMapper $propertyPathMapper;

    public function __construct(
        PropertyPathMapper $propertyPathMapper,
        ConfigManager $configManager,
        array $configurators = [],
        AuthorizationCheckerInterface $authorizationChecker = null
    ) {
        parent::__construct($configManager, $configurators, $authorizationChecker);
        $this->propertyPathMapper = $propertyPathMapper;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder->setDataMapper($this->propertyPathMapper);
    }
}