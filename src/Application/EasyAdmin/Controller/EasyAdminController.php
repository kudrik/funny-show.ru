<?php

declare(strict_types=1);

namespace App\Application\EasyAdmin\Controller;

use App\Entity\Catalog;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PropertyAccess\PropertyAccess;

class EasyAdminController extends \EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController
{
    protected function editAction(): Response
    {
        if ($this->request->isXmlHttpRequest() && $property = $this->request->get('fieldname')) {

            $this->dispatch(EasyAdminEvents::PRE_EDIT);

            $value = $this->request->get('value');
            $easyadmin = $this->request->attributes->get('easyadmin');
            $entity = $easyadmin['item'];

            $this->updateEntityProperty($entity, $property, $value);

            return new Response($value);
        }

        return parent::editAction();
    }

    protected function redirectToReferrer(): Response
    {
        $refererAction = $this->request->query->get('action');

        // from new|edit action, redirect to edit if possible
        if (in_array($refererAction, array('new', 'edit')) && $this->isActionAllowed('edit')) {
            return $this->redirectToRoute('easyadmin', array(
                'action' => 'edit',
                'entity' => $this->entity['name'],
                'menuIndex' => $this->request->query->get('menuIndex'),
                'submenuIndex' => $this->request->query->get('submenuIndex'),
                'id' => ('new' === $refererAction)
                    ? PropertyAccess::createPropertyAccessor()->getValue($this->request->attributes->get('easyadmin')['item'], $this->entity['primary_key_field_name'])
                    : $this->request->query->get('id'),
            ));
        }

        return parent::redirectToReferrer();
    }

    protected function copyAction(): Response
    {
        $easyadmin = $this->request->attributes->get('easyadmin');

        $entityRepository = $this->getDoctrine()->getRepository($easyadmin['entity']['class']);

        if ($entity = $entityRepository->find((int) $this->request->get('id'))) {
            $newEntity = clone $entity;
            $this->getDoctrine()->getManager()->persist($newEntity);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('easyadmin', array(
                'action' => 'edit',
                'entity' => $this->entity['name'],
                'menuIndex' => $this->request->query->get('menuIndex'),
                'submenuIndex' => $this->request->query->get('submenuIndex'),
                'id' => $newEntity->getId(),
            ));
        }

        throw new NotFoundHttpException('Entity not found');
    }
}