<?php

declare(strict_types=1);

namespace App\Application\NestedSets;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

trait NestedSetsTrait
{
    private ?self $parent = null;

    public Collection $children;

    public int $lft = 0;

    public int $rgt = 0;

    public int $lvl = 0;

    public function addChild(self $child): self
    {
        $this->children->add($child);

        return $this;
    }

    public function removeChild(self $child): self
    {
        $this->children->removeElement($child);

        return $this;
    }

    public function setParent(?self $parent): self
    {
        if ($parent && $parent->getId() === $this->getId()) {
            throw new \Exception('Parent can not be parent to itself');
        }

        $this->parent = $parent;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }
}