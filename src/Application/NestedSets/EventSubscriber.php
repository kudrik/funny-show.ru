<?php

declare(strict_types=1);

namespace App\Application\NestedSets;

use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class EventSubscriber implements \Doctrine\Common\EventSubscriber
{
    protected bool $singleton = true;

    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
            Events::postUpdate,
            Events::postRemove,
        ];
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $this->build($args);
    }

    public function postUpdate(LifecycleEventArgs $args): void
    {
        $this->build($args);
    }

    public function postRemove(LifecycleEventArgs $args): void
    {
        $this->build($args);
    }

    private function supports($entity): bool
    {
        return in_array(NestedSetsTrait::class, class_uses($entity));
    }

    /**
     * @param NestedSetsTrait[] $items
     * @param int $lft
     * @param int $level
     * @return int
     */
    private function tree(iterable $items, int $lft, int $level): int
    {
        $level++;

        foreach ($items as $item) {

            //echo $item->getName() . ' - ' . $item->getPosition() .' - ' . $item->getLft(). ':'.$item->getRgt() .'<br>';
            $lft++;

            $item->lft = $lft;

            $item->lvl = $level;

            $lft = $this->tree($item->children, $lft, $level);

            $item->rgt = $lft;
        }

        return $lft + 1;
    }

    public function build(LifecycleEventArgs $args): void
    {
        /** @var NestedSetsTrait $entity */
        $entity = $args->getObject();

        if ($this->supports($entity) && $this->singleton) {

            $this->singleton = false;

            /** @var NestedSetsTrait[] $items */
            $items = $args->getObjectManager()->getRepository(get_class($entity))->findBy(['parent' => null], ['position' => 'asc']);
            $this->tree($items, 0, 0);
            $args->getObjectManager()->flush();
        }
    }
}