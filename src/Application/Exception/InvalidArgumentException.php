<?php

declare(strict_types=1);

namespace App\Application\Exception;

use Throwable;

class InvalidArgumentException extends \InvalidArgumentException
{
    private object $object;

    private string $property;

    public function __construct(object $object, string $property, string $message, ?Throwable $previous = null)
    {
        $this->property = $property;

        $this->object = $object;

        parent::__construct($message, 0, $previous);
    }

    public function getProperty(): string
    {
        return $this->property;
    }

    public function getObjectName(): string
    {
        return get_class($this->object);
    }
}