<?php

declare(strict_types=1);

namespace App\Application\Pagination;

class Pagination
{
    private array $pages = [];

    private int $current;

    public function __construct(
        int $total,
        int $current,
        int $perPage,
        string $baseRouteName,
        array $baseRouteParameters
    ) {
        if ($current < 1) {
            $current = 1;
        }

        if ($perPage < 1) {
            $perPage = 10;
        }

        $this->current = $current;

        $totalPages = $total / $perPage;
        if ($totalPages !== (int)$totalPages) {
            $totalPages = (int)$totalPages + 1;
        }

        for ($p = 1; $p <= $totalPages; $p++) {
            $parameters = $baseRouteParameters;
            $parameters['page'] = $p;

            $this->pages[] = new Item($baseRouteName, $parameters, (string)$p, $this->current === $p);
        }
    }

    public function getPages(): array
    {
        return $this->pages;
    }

    public function getCurrent(): int
    {
        return $this->current;
    }
}
