<?php

declare(strict_types=1);

namespace App\Application\Pagination;

class Item
{
    private string $routeName;

    private array $routeParameters;

    private string $name;

    private bool $current = false;

    public function __construct(string $routeName, array $routeParameters, string $name, bool $current)
    {
        $this->routeName = $routeName;
        $this->routeParameters = $routeParameters;
        $this->name = $name;
        $this->current = $current;
    }

    public function getRouteName(): string
    {
        return $this->routeName;
    }

    public function getRouteParameters(): array
    {
        return $this->routeParameters;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isCurrent(): bool
    {
        return $this->current;
    }
}