<?php

declare(strict_types=1);

namespace App\Application\Imageable;

use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension
{
    private ImageableUrlGenerator $imageableUrlGenerator;

    public function __construct(ImageableUrlGenerator $imageableUrlGenerator)
    {
        $this->imageableUrlGenerator = $imageableUrlGenerator;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('imageable_asset', [$this, 'getImageableAssetPath']),
        ];
    }

    /**
     * @param ImageableTrait $object
     */
    public function getImageableAssetPath(object $object, ?string $filter): string
    {
        if ($filter) {
            return $this->imageableUrlGenerator->getAssetPath($object, $filter);
        }

        return $this->imageableUrlGenerator->getOrigAssetPath($object);
    }
}