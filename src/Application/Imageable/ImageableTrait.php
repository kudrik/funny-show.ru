<?php

declare(strict_types=1);

namespace App\Application\Imageable;

use DateTime;
use Symfony\Component\HttpFoundation\File\UploadedFile;

trait ImageableTrait
{
    private ?UploadedFile $imageFile = null;

    private ?string $imageFileName = null;

    private ?DateTime $imageUpdatedAt = null;

    private function getImageExt(): string
    {
        return 'jpg';
    }

    public function getImageFile(): ?UploadedFile
    {
        return $this->imageFile;
    }

    public function setImageFile(UploadedFile $imageFile): self
    {
        $this->imageFile = $imageFile;

        $this->imageUpdatedAt = new DateTime();

        if (!$this->imageFileName) {
            if (method_exists($this, 'getId') && $this->getId()) {
                $this->imageFileName = $this->getId().'.'.$this->getImageExt();
            } else {
                $this->imageFileName = uniqid('', false).'.'.$this->getImageExt();
            }
        }

        return $this;
    }

    public function getImageFileName(): ?string
    {
        return $this->imageFileName;
    }

    public function getImagePath(): string
    {
        return strtolower(basename(str_replace('\\', '/', get_class($this)))).'/';
    }

    public function getImageBaseName(): ?string
    {
        return $this->getImageFileName() ? $this->getImagePath().$this->getImageFileName() : null;
    }

    public function getVersion(): string
    {
        return $this->imageUpdatedAt ? (string) $this->imageUpdatedAt->getTimestamp() : '';
    }
}