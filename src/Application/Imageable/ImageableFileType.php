<?php

declare(strict_types=1);

namespace App\Application\Imageable;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImageableFileType extends AbstractType
{
    private ImageableUrlGenerator $imageableUrlGenerator;

    public function __construct(ImageableUrlGenerator $imageableUrlGenerator)
    {
        $this->imageableUrlGenerator = $imageableUrlGenerator;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('getter', null);
    }

    public function getParent(): string
    {
        return FileType::class;
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        /** @var ImageableTrait $object */
        $object = $form->getParent() ? $form->getParent()->getData() : null;

        if ($object && $options['getter']) {
            $getter = $options['getter'];
            if (method_exists($object, $getter)) {
                $object = $object->$getter();
            } elseif (property_exists($object, $getter)) {
                $object = $object->$getter;
            } else {
                throw new \Exception(get_class($object).': getter '.$getter.' not found');
            }
        }

        $view->vars['attr'] = ['accept' => 'image/*'];

        if ($object && $object->getImageFileName()) {

            $view->vars['required'] = false;

            $imageSmallSrc = $this->imageableUrlGenerator->getAssetPath($object, 'small');

            $imageLargeSrc = $this->imageableUrlGenerator->getAssetPath($object, 'medium');

            $imageId = uniqid();

            $view->vars['help'] = '
             <div id="easyadmin-lightbox-'.$imageId.'" class="easyadmin-lightbox">
                <img src="'.$imageLargeSrc.'" alt="">
             </div>
             <a 
                class="easyadmin-thumbnail" 
                data-featherlight="#easyadmin-lightbox-'.$imageId.'" 
                data-featherlight-close-on-click="anywhere" 
                style="display:block; margin-top: 1em; width: 4em;">
                <img style="max-width:100%;" src="'.$imageSmallSrc.'" alt="">
             </a>';
        }
    }
}