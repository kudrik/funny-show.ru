<?php

declare(strict_types=1);

namespace App\Application\Imageable;

use Liip\ImagineBundle\Imagine\Cache\CacheManager;

class ImageableUrlGenerator
{
    private CacheManager $cacheManager;

    private string $imagesBasePath;

    public function __construct(CacheManager $cacheManager, string $imagesBasePath)
    {
        $this->cacheManager = $cacheManager;
        $this->imagesBasePath = $imagesBasePath;
    }

    /**
     * @param ImageableTrait $object
     */
    public function getAssetPath(object $object, string $filter): string
    {
        $url = $this->cacheManager->getBrowserPath($this->imagesBasePath.$object->getImageBaseName(), $filter);

        return trim($url).'?v='.$object->getVersion();
    }

    /**
     * @param ImageableTrait $object
     */
    public function getOrigAssetPath(object $object): string
    {
        return $this->imagesBasePath.$object->getImageBaseName().'?v='.$object->getVersion();
    }
}