<?php

declare(strict_types=1);

namespace App\Application\Imageable;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Liip\ImagineBundle\Imagine\Cache\CacheManager as ImagineCacheManager;
use Liip\ImagineBundle\Imagine\Filter\FilterConfiguration;
use Liip\ImagineBundle\Service\FilterService as ImagineFilterService;

class ImageableSubscriber implements EventSubscriber
{
    private string $imagesBaseDir;

    private string $imagesBasePath;

    private ImagineCacheManager $imagineCacheManager;

    private FilterConfiguration $filterConfiguration;

    private ImagineFilterService $imagineFilterService;

    public function __construct(
        string $imagesBaseDir,
        string $imagesBasePath,
        ImagineCacheManager $imagineCacheManager,
        ImagineFilterService $imagineFilterService,
        FilterConfiguration $filterConfiguration
    ) {
        $this->imagesBaseDir = $imagesBaseDir;
        $this->imagesBasePath = $imagesBasePath;
        $this->imagineCacheManager = $imagineCacheManager;
        $this->filterConfiguration = $filterConfiguration;
        $this->imagineFilterService = $imagineFilterService;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
            Events::postUpdate,
            Events::preRemove,
        ];
    }

    /**
     * @param ImageableTrait $entity
     */
    private function uploadImage(object $entity): void
    {
        if (!$this->isImageable($entity) || !$entity->getImageFile()) {
            return;
        }

        $source = $entity->getImageFile()->getRealPath();

        if ($source && getimagesize($source)) {
            $entity->getImageFile()->move($this->imagesBaseDir.$entity->getImagePath(), $entity->getImageFileName());
            $this->warmUpCache($entity);
        }
    }

    /** @var ImageableTrait $entity */
    private function removeImage(object $entity): void
    {
        if (!$this->isImageable($entity)) {
            return;
        }

        @unlink($this->imagesBaseDir.$entity->getImageBaseName());
        $this->clearCache($entity);
    }

    private function isImageable(object $entity): bool
    {
        return in_array(ImageableTrait::class, class_uses($entity));
    }

    /**
     * @param ImageableTrait $entity
     */
    private function warmUpCache(object $entity): void
    {
        if (!$entity->getImageBaseName()) {
            return;
        }

        $this->clearCache($entity);

        foreach ($this->filterConfiguration->all() as $filterName => $filter) {
            $this->imagineFilterService->getUrlOfFilteredImage(
                $this->imagesBasePath.$entity->getImageBaseName(),
                $filterName
            );
        }
    }

    /**
     * @param ImageableTrait $entity
     */
    private function clearCache(object $entity): void
    {
        if (!$entity->getImageBaseName()) {
            return;
        }

        $this->imagineCacheManager->remove($this->imagesBasePath.$entity->getImageBaseName());
    }

    private function getAllEmbedded(LifecycleEventArgs $args): array
    {
        $entity = $args->getObject();

        //get metadata for the entity
        $classMetaData = $args->getObjectManager()->getClassMetadata(get_class($entity));

        $reflectionClass = $classMetaData->getReflectionClass();

        $result = [];
        //find all embedded objects
        foreach ($classMetaData->embeddedClasses as $embeddedName => $embeddedClass) {
            $property = $reflectionClass->getProperty($embeddedName);
            $property->setAccessible(true);
            $result[] = $property->getValue($entity);
        }

        return $result;
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $this->uploadImage($args->getObject());

        foreach ($this->getAllEmbedded($args) as $embedded) {
            $this->uploadImage($embedded);
        }
    }

    public function postUpdate(LifecycleEventArgs $args): void
    {
        $this->uploadImage($args->getObject());

        foreach ($this->getAllEmbedded($args) as $embedded) {
            $this->uploadImage($embedded);
        }
    }

    public function preRemove(LifecycleEventArgs $args): void
    {
        $this->removeImage($args->getObject());

        foreach ($this->getAllEmbedded($args) as $embedded) {
            $this->removeImage($embedded);
        }
    }
}