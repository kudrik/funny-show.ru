<?php

declare(strict_types=1);

namespace App\Application\Search\Entity;

interface SearchableInterface
{
    public function getId(): ?int;

    public function getForSearch(): array;
}
