<?php

declare(strict_types=1);

namespace App\Application\Search;

use App\Application\Search\Entity\SearchableInterface;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class EventSubscriber implements \Doctrine\Common\EventSubscriber
{
    private SearchService $searchService;

    public function __construct(SearchService $searchService)
    {
        $this->searchService = $searchService;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
            Events::postUpdate,
        ];
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $this->process($args);
    }

    public function postUpdate(LifecycleEventArgs $args): void
    {
        $this->process($args);
    }

    private function supports(object $entity): bool
    {
        return $entity instanceof SearchableInterface && $entity->getId();
    }

    private function process(LifecycleEventArgs $args): void
    {
        /** @var SearchableInterface $entity */
        $entity = $args->getObject();

        if (!$this->supports($entity)) {
            return;
        }

        $this->searchService->update($entity);
    }
}