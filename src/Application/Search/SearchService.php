<?php

declare(strict_types=1);

namespace App\Application\Search;

use App\Application\Search\Entity\SearchableInterface;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Table;
use Doctrine\ORM\EntityManagerInterface;

class SearchService
{
    private Connection $connection;

    private EntityManagerInterface $entityManager;

    public function __construct(Connection $connection, EntityManagerInterface $entityManager)
    {
        $this->connection = $connection;
        $this->entityManager = $entityManager;
    }

    private function createTable(SearchableInterface $entity): void
    {
        $tableName = $this->getTableName(get_class($entity));

        if ($this->connection->getSchemaManager()->tablesExist($tableName)) {
            return;
        }

        $metadata = $this->entityManager->getClassMetadata(get_class($entity));

        $table = new Table($tableName, []);
        $table->addColumn('parent_id', 'integer');
        $table->addColumn('tekst', 'text');
        $table->setPrimaryKey(['parent_id']);
        $table->addIndex(['tekst'], null, ['fulltext']);
        $table->addForeignKeyConstraint($metadata->getTableName(), ['parent_id'], ['id'], ['onDelete' => 'CASCADE']);

        $this->connection->getSchemaManager()->createTable($table);
    }

    private function getTableName(string $className): string
    {
        $metadata = $this->entityManager->getClassMetadata($className);

        return $metadata->getTableName().'_search';
    }

    public function update(SearchableInterface $entity): void
    {
        $this->createTable($entity);

        $tableName = $this->getTableName(get_class($entity));

        $this->connection->delete($tableName, ['parent_id' => $entity->getId()]);

        $tekst = implode(' ', $entity->getForSearch());
        $tekst = mb_strtolower($tekst);
        $tekst = str_replace('&nbsp;', ' ', $tekst);
        $tekst = preg_replace('/^\s+|\s+$|\s+(?=\s)/', '', $tekst);

        $this->connection->insert(
            $tableName,
            [
                'parent_id' => $entity->getId(),
                'tekst' => $tekst,
            ]
        );
    }

    public function search(string $className, ?string $search, int $offset = 0, int $limit = 100): array
    {
        $search = trim((string)$search);
        $search = mb_strtolower($search);

        if (!$search) {
            return [];
        }

        $tableName = $this->getTableName($className);

        $result = $this->connection
            ->executeQuery(
                'SELECT s.parent_id FROM '.$tableName.' AS s
                 WHERE MATCH(s.tekst) AGAINST(?) LIMIT '.$offset.','.$limit,
                [$search]
            )
            ->fetchAll();

        $ids = array_map(fn(array $row) => (int) $row['parent_id'], $result);

        unset($result);

        return $ids;
    }
}