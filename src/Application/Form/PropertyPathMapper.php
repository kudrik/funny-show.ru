<?php

declare(strict_types=1);

namespace App\Application\Form;

use App\Application\Exception\InvalidArgumentException;
use Symfony\Component\Form\FormError;
use Symfony\Contracts\Translation\TranslatorInterface;

class PropertyPathMapper extends \Symfony\Component\Form\Extension\Core\DataMapper\PropertyPathMapper
{
    private TranslatorInterface $translator;

    public function setTranslator(TranslatorInterface $translator): void
    {
        $this->translator = $translator;
    }

    public function mapFormsToData(iterable $forms, &$data)
    {
        try {
            parent::mapFormsToData($forms, $data);
        } catch (InvalidArgumentException $exception) {
            foreach ($forms as $form) {
                if ($form->getName() === $exception->getProperty()) {
                    $message = $this->translator->trans($exception->getMessage());
                    $form->addError(new FormError($message));
                }
            }
        }
    }
}