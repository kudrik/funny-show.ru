<?php

declare(strict_types=1);

namespace App\Application\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class AntiSpamTypeExtension extends AbstractTypeExtension
{
    private const FIELD_FROM = 'tralala';

    private const FIELD_TO = 'pumpurum';

    private const SESSION_POSTFIX = '_antispam';

    private SessionInterface $session;

    private TranslatorInterface $translator;

    public function __construct(SessionInterface $session, TranslatorInterface $translator)
    {
        $this->session = $session;
        $this->translator = $translator;
    }

    public static function getExtendedTypes(): iterable
    {
        return [FormType::class];
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault('antispam', false);
    }

    public function onSubmit(FormEvent $event): void
    {
        $form = $event->getForm();

        $secret_from_session = $this->session->get($this->getSessionKey($form));

        if (!$secret_from_session || $form->get(self::FIELD_TO)->getData() != $secret_from_session) {
            $form->addError(new FormError($this->translator->trans('form.antispam.error')));
        }
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (!$options['antispam']) {
            return;
        }

        $builder->add(self::FIELD_FROM, HiddenType::class, ['mapped' => false]);

        $builder->add(self::FIELD_TO, HiddenType::class, ['mapped' => false]);

        $builder->addEventListener(FormEvents::SUBMIT, [$this, 'onSubmit']);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if (!$options['antispam'] || $form->isSubmitted()) {
            return;
        }

        $key = $this->getSessionKey($form);

        if ($this->session->has($key)) {
            $secret_key_value = $this->session->get($key);
        } else {
            $secret_key_value = md5('trolololalilala'.time());
            $this->session->set($key, $secret_key_value);
        }

        $form->get(self::FIELD_FROM)->setData($secret_key_value);

        $this->setTriggerField($form, $options['antispam']);
    }

    private function getSessionKey(FormInterface $form): string
    {
        return $form->getName().'_'.self::SESSION_POSTFIX;
    }

    private function setTriggerField(FormInterface $form, string $name): void
    {
        $triggerField = $form->get($name);

        $triggerFieldOptions = $triggerField->getConfig()->getOptions();

        $fieldIdPrefix = ($form->getParent() ? $form->getParent()->getName().'_' : '') . $form->getName().'_';

        $triggerFieldOptions['attr']['onclick'] = 'document.getElementById("'.$fieldIdPrefix.self::FIELD_TO.'").value = document.getElementById("'.$fieldIdPrefix.self::FIELD_FROM.'").value;';
        $triggerFieldOptions['attr']['onkeypress'] = $triggerFieldOptions['attr']['onclick'];

        $form->add(
            $triggerField->getName(),
            get_class($triggerField->getConfig()->getType()->getInnerType()),
            $triggerFieldOptions
        );
    }
}