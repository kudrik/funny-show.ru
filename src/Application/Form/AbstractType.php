<?php

declare(strict_types=1);

namespace App\Application\Form;

use Symfony\Component\Form\FormBuilderInterface;

class AbstractType extends \Symfony\Component\Form\AbstractType
{
    private PropertyPathMapper $propertyPathMapper;

    public function __construct(PropertyPathMapper $propertyPathMapper)
    {
        $this->propertyPathMapper = $propertyPathMapper;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->setDataMapper($this->propertyPathMapper);
    }
}