<?php

declare(strict_types=1);

namespace App\Application\Breadcrumbs;

class Breadcrumbs
{
    private array $items = [];

    public function add(string $name, ?string $url): self
    {
        $this->items[] = new Item($name, $url);

        return $this;
    }

    public function getItems(): array
    {
        return $this->items;
    }
}