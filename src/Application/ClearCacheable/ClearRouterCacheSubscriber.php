<?php

declare(strict_types=1);

namespace App\Application\ClearCacheable;

use Doctrine\Common\EventSubscriber;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Bundle\FrameworkBundle\CacheWarmer\RouterCacheWarmer;

class ClearRouterCacheSubscriber implements EventSubscriber
{
    private string $cacheDir;

    public function __construct(string $cacheDir)
    {
        $this->cacheDir = $cacheDir;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
            Events::postUpdate,
            Events::postRemove
        ];
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $this->warmUp($args);
    }

    public function postUpdate(LifecycleEventArgs $args): void
    {
        $this->warmUp($args);
    }

    public function postRemove(LifecycleEventArgs $args): void
    {
        $this->warmUp($args);
    }

    private function warmUp(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof ClearCacheableInterface) {
            @unlink($this->cacheDir. '/url_matching_routes.php');
            @unlink($this->cacheDir. '/url_generating_routes.php');
        }
    }
}