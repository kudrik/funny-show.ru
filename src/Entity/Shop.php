<?php

declare(strict_types=1);

namespace App\Entity;

use App\Application\Imageable\ImageableTrait;
use App\Application\Sortable\SortableTrait;

class Shop
{
    use ImageableTrait;
    use SortableTrait;

    private ?int $id = null;
    private ?Page $page = null;
    public bool $published = true;
    public ?string $name = null;
    public int $price = 0;
    public int $amount = 1;
    public ?string $description = null;
    public ?string $vendor = null;
    public ?string $model = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPage(): ?Page
    {
        return $this->page;
    }

    public function setPage(Page $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getMeta(): MetaInfo
    {
        return new MetaInfo($this->name, $this->description, null, null);
    }

    public function __clone() {
        $this->id = null;
        $this->imageFileName = null;
        $this->name = 'Копия '.$this->name;
        $this->position = 0;
    }
}