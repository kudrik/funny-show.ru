<?php

declare(strict_types=1);

namespace App\Entity;

use App\Application\ClearCacheable\ClearCacheableInterface;
use App\Application\Exception\InvalidArgumentException;
use App\Application\Sortable\SortableTrait;

class Menu implements ClearCacheableInterface
{
    use SortableTrait;

    private ?int $id = null;

    private ?string $name = null;

    private ?string $url = null;

    private ?Page $page = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name)
    {
        if (!$name && !$this->getPage()) {
            throw new InvalidArgumentException($this, 'name','Name or Page should be provided');
        }

        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getPage(): ?Page
    {
        return $this->page;
    }

    public function setPage(?Page $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->getName() ?? $this->getPage()->getName();
    }
}
