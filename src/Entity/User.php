<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Traits\TimestampableTrait;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface, \Serializable
{
    use TimestampableTrait;

    public const ROLES = ['ROLE_ADMIN' => 'ROLE_ADMIN', 'ROLE_USER' => 'ROLE_USER'];

    private ?int $id = null;

    private ?string $email = null;

    private ?string $password = null;

    private array $roles = ['ROLE_USER'];

    private ?string $restore = null;

    private ?\DateTime $restoreTo = null;

    private ?string $plainPassword = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->email;
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function eraseCredentials()
    {
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    public function getRestore(): ?string
    {
        return $this->restore;
    }

    public function setRestore(?string $restore): self
    {
        $this->restore = $restore;

        return $this;
    }

    public function getRestoreTo(): ?\DateTime
    {
        return $this->restoreTo;
    }

    public function setRestoreTo(?\DateTime $restoreTo): self
    {
        $this->restoreTo = $restoreTo;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }


    public function setPlainPassword(string $password): self
    {
        $this->plainPassword = $password;

        $this->setUpdatedAtValue();

        return $this;
    }

    public function getRolesString(): string
    {
        return implode(', ', $this->getRoles());
    }

    public function __toString(): string
    {
        return (string) $this->email;
    }
}
