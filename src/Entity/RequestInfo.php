<?php

declare(strict_types=1);

namespace App\Entity;

class RequestInfo
{
    private string $ip;

    private ?string $referrer;

    public function __construct(string $ip, ?string $referrer)
    {
        $this->ip = $ip;
        $this->referrer = $referrer;
    }

    public function getIp(): string
    {
        return $this->ip;
    }

    public function getReferrer(): ?string
    {
        return $this->referrer;
    }
}