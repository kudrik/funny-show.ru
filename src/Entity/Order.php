<?php

declare(strict_types=1);

namespace App\Entity;

use App\Application\Exception\InvalidArgumentException;
use App\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Order
{
    use TimestampableTrait;

    public ?int $id = null;

    private ?Client $client = null;

    public string $theme = 'Консультация';

    public ?string $description = null;

    private ?Catalog $catalog = null;

    private ?RequestInfo $requestInfo = null;

    public bool $done = false;

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getCatalog(): ?Catalog
    {
        return $this->catalog;
    }

    public function setCatalog(Catalog $catalog): self
    {
        $this->catalog = $catalog;

        $this->theme = $catalog->name ?: '#'.$catalog->getId();

        return $this;
    }

    public function getRequestInfo(): ?RequestInfo
    {
        return $this->requestInfo;
    }

    public function setRequestInfo(RequestInfo $requestInfo): self
    {
        $this->requestInfo = $requestInfo;

        return $this;
    }
}