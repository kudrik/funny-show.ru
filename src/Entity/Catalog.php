<?php

declare(strict_types=1);

namespace App\Entity;

use App\Application\Imageable\ImageableTrait;
use App\Application\Sortable\SortableTrait;
use App\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Catalog
{
    use TimestampableTrait;
    use SortableTrait;
    use ImageableTrait;

    private ?int $id = null;
    public ?string $name = null;
    public ?string $description = null;
    public ?string $anonse = null;
    public bool $published = true;
    public bool $showOnMainPage = false;
    public int $price = 0;

    /** @var CatalogItem[] */
    private Collection $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(CatalogItem $item): self
    {
        $item->catalog = $this;

        $this->items->add($item);

        return $this;
    }

    public function removeItem(CatalogItem $item): self
    {
        $this->items->removeElement($item);

        return $this;
    }

    public function getMeta(): MetaInfo
    {
        return new MetaInfo($this->name, $this->description, null, null);
    }

    public function __clone()
    {
        $this->id = null;
        $this->name = 'Копия '.$this->name;
        $this->showOnMainPage = false;
        $this->createdAt = null;
        $this->updatedAt = null;
        $this->position = 0;
        $this->items = new ArrayCollection();
        $this->imageFileName = null;
    }
}