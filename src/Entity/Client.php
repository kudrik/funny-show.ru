<?php

declare(strict_types=1);

namespace App\Entity;

use App\Application\Exception\InvalidArgumentException;

class Client
{
    private ?int $id = null;

    private ?string $name = null;

    private ?string $phone = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        preg_match_all('/\d/', $phone, $match);
        $phone = implode('', $match[0]);

        if (strlen($phone) < 8) {
            throw new InvalidArgumentException($this, 'phone','Invalid phone');
        }

        $this->phone = $phone;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $value): self
    {
        $value = trim($value);

        if (!$value) {
            throw new InvalidArgumentException($this, 'name', 'The field can not be empty');
        }

        $this->name = $value;

        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->name;
    }
}
