<?php

declare(strict_types=1);

namespace App\Entity;

use App\Application\ClearCacheable\ClearCacheableInterface;
use App\Application\Imageable\ImageableTrait;
use App\Application\NestedSets\NestedSetsTrait;
use App\Application\Sortable\SortableTrait;
use App\Entity\Traits\TimestampableTrait;
use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

class Page implements ClearCacheableInterface
{
    use TimestampableTrait;
    use SortableTrait;
    use NestedSetsTrait;
    use ImageableTrait;

    private ?int $id = null;

    private ?string $name = null;

    private ?string $url = null;

    public ?string $anonse = null;

    public ?string $description = null;

    public MetaInfo $meta;

    /** @var Page[] */
    public Collection $children;

    /** @var Photo[] */
    public Collection $photos;

    /** @var Shop[] */
    public Collection $shop;

    public ?Menu $menu = null;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->photos = new ArrayCollection();
        $this->shop = new ArrayCollection();
        $this->meta = new MetaInfo(null, null, null, null);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        if (!$this->getUrl()) {
            $this->setUrl($this->name);
        }

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        if (!$url) {
            return $this;
        }

        $url = trim($url);

        $url = Slugify::create()->slugify($url);

        $this->url = $url;

        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->getName();
    }

    /** @return self[] */
    public function getAllParents(): array
    {
        $parents = array();

        $parent = $this;

        while ($parent) {
            if ($parent = $parent->getParent()) {
                array_unshift($parents, $parent);
            }
        }

        return $parents;
    }
}