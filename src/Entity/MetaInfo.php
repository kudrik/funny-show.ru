<?php

declare(strict_types=1);

namespace App\Entity;

class MetaInfo
{
    public ?string $title = null;

    public ?string $description = null;

    public ?string $keywords = null;

    public ?string $image = null;

    public ?string $canonical = null;

    public function __construct(?string $title, ?string $description, ?string $keywords, ?string $image, ?string $canonical = null)
    {
        $this->title = $title;
        $this->description = $description ? strip_tags($description) : null;
        $this->keywords = $keywords;
        $this->image = $image;
        $this->canonical = $canonical;
    }

    public function createWithDefault(array $defaultMeta): self
    {
        $title = $this->title ?: $defaultMeta['title'];
        $description = $this->description ?: $defaultMeta['description'];
        $keywords = $this->keywords ?: $defaultMeta['keywords'];

        return new self($title, $description, $keywords, $this->image, $this->canonical);
    }

    public function createWithDefaultTitle(string $defaultTitle): self
    {
        return new self($this->title ?: $defaultTitle, $this->description, $this->keywords, $this->image, $this->canonical);
    }

    public function createWithImage(string $image): self
    {
        return new self($this->title, $this->description, $this->keywords, $image, $this->canonical);
    }

    public function createWithCanonical(string $canonical): self
    {
        return new self($this->title, $this->description, $this->keywords, $this->image, $canonical);
    }
}