<?php

declare(strict_types=1);

namespace App\Entity;

use App\Application\Imageable\ImageableTrait;
use App\Application\Sortable\SortableTrait;

class Photo
{
    use SortableTrait;
    use ImageableTrait;

    private ?int $id = null;
    public ?string $name = null;
    public ?Page $page = null;

    public function getId(): ?int
    {
        return $this->id;
    }
}