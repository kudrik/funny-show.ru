<?php

declare(strict_types=1);

namespace App\Entity\Traits;

use DateTime;

trait TimestampableTrait
{
    public ?DateTime $createdAt = null;

    public ?DateTime $updatedAt = null;

    public function setCreatedAtValue(): self
    {
        $this->createdAt = new DateTime();
        $this->setUpdatedAtValue();

        return $this;
    }

    public function setUpdatedAtValue(): self
    {
        $this->updatedAt = new DateTime();

        return $this;
    }
}