<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Tag
{
    private ?int $id = null;

    private ?string $name = null;

    private Collection $catalog;

    public function __construct()
    {
        $this->catalog = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $name = trim($name);

        if (!$name) {
            throw new \Exception('tag value can not be empty');
        }

        $this->name = $name;

        return $this;
    }

    public function getCatalog(): Collection
    {
        return $this->catalog;
    }

    public function __toString(): string
    {
        return (string) $this->getName();
    }
}