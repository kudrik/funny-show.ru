<?php

declare(strict_types=1);

namespace App\Entity;

use App\Application\Imageable\ImageableTrait;
use App\Application\Sortable\SortableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

class CatalogItem
{
    use ImageableTrait;
    use SortableTrait;

    public ?int $id = null;

    public ?Catalog $catalog = null;

    public function getImagePath(): string
    {
        return 'catalog/';
    }

    public function getMaxPositionCriteria(): Criteria
    {
        return (new Criteria())->where(Criteria::expr()->eq('catalog', $this->catalog));
    }
}