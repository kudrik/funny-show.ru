<?php

namespace App\Routing;

use App\Controller\CatalogController;
use App\Controller\PageController;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use App\Entity\Page;

class DynamicLoader extends Loader
{
    private bool $isLoaded = false;

    private ManagerRegistry $managerRegistry;

    private string $siteHost;

    public function __construct(ManagerRegistry $managerRegistry, string $siteHost)
    {
        $this->managerRegistry = $managerRegistry;
        $this->siteHost = $siteHost;
    }

    public function load($resource, string $type = null): RouteCollection
    {
        if (true === $this->isLoaded) {
            throw new \RuntimeException('Do not add the "extra" loader twice');
        }

        $routes = new RouteCollection();

        $this->pages($routes);

        $this->isLoaded = true;

        return $routes;
    }

    private function pages(RouteCollection $routes): void
    {
        /** @var Page $page */
        foreach ($this->managerRegistry->getRepository(Page::class)->findAll() as $page) {

            $urls = array_map(fn(Page $parent) => $parent->getUrl(), $page->getAllParents());

            $urls[] = trim($page->getUrl());

            $url = mb_strtolower('/' . implode('/', $urls));

            //exclude main page
            if ($url === '/index') {
                $url = '/';
            }

            $route = new Route($url, [
                '_controller' => PageController::class.':index',
                'pageId' => $page->getId(),
            ]);

            $routes->add('page' . $page->getId(), $route);

            $this->shop($routes, $page, $url);
        }
    }

    private function shop(RouteCollection $routes, Page $page, string $url): void
    {
        if ($page->shop->count() < 1) {
            return ;
        }

        $route = new Route($url.'/{id}', [
            '_controller' => 'App\Controller\ShopController:item',
        ]);

        $routes->add('shop'.$page->getId(), $route);

        /*
        * @TODO remove me
       foreach ($page->shop as $shop) {

           if (!$shop->published) {
               continue;
           }

           $route = new Route($url.'/'.$shop->getId(), [
               '_controller' => 'App\Controller\ShopController:item',
               'id' => $shop->getId(),
           ]);

           $routes->add('shop' . $shop->getId(), $route);
       }
       */
    }

    public function supports($resource, string $type = null)
    {
        return 'extra' === $type;
    }
}