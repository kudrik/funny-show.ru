<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Entity\Order;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class OrderSubscriber implements EventSubscriber
{
    protected MailerInterface $mailer;
    protected Address $mailFrom;
    /** @var Address[]  */
    protected array $mailToList;
    protected string $siteName;

    public function __construct(MailerInterface $mailer, string $mailFrom, array $mailToList, string $siteName)
    {
        $this->mailer = $mailer;
        $this->mailFrom = new Address($mailFrom);
        $this->mailToList = Address::createArray($mailToList);
        $this->siteName = $siteName;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
        ];
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $order = $args->getObject();

        if (!($order instanceof Order)) {
            return;
        }

        $email = (new TemplatedEmail())
            ->from($this->mailFrom)
            ->to(...$this->mailToList)
            ->subject('Заказ с сайта ' . $this->siteName)
            ->htmlTemplate('order/email.html.twig')
            ->context(['order' => $order]);

        $this->mailer->send($email);
    }
}