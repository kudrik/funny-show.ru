<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Menu;
use App\Repository\MenuRepository;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class MenuBuilder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    private FactoryInterface $factory;

    private MenuRepository $menuRepository;

    public function __construct(FactoryInterface $factory, MenuRepository $menuRepository)
    {
        $this->factory = $factory;
        $this->menuRepository = $menuRepository;
    }

    public function mainMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'navbar-nav');

        /** @var Menu $item */
        foreach ($this->menuRepository->findBy([], ['position' => 'ASC']) as $item) {
            $options = [
                'attributes' =>  ['class' => 'nav-item'],
                'linkAttributes' => ['class' => 'nav-link'],
                'label' => $item->getFullName(),
            ];

            if ($item->getUrl()) {
                $name = $item->getUrl();
                $options['uri'] = $item->getUrl();
            } else {
                $page = $item->getPage();
                $options['route'] = 'page' . $page->getId();
                $name = $page->getId();
            }

            $menu->addChild($name, $options);
        }

        return $menu;
    }
}