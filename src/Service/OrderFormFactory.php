<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Catalog;
use App\Entity\Order;
use App\Form\OrderType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class OrderFormFactory
{
    private FormFactoryInterface $formFactory;

    private UrlGeneratorInterface $urlGenerator;

    private Order $order;

    public function __construct(FormFactoryInterface $formFactory, UrlGeneratorInterface $urlGenerator)
    {
        $this->formFactory = $formFactory;
        $this->urlGenerator = $urlGenerator;
        $this->order = new Order();
    }

    public function setCatalog(Catalog $catalog): void
    {
        $this->order->setCatalog($catalog);
    }

    public function create(): FormInterface
    {
        $title = 'Задать вопрос';
        $catalog = $this->order->getCatalog();
        if ($catalog) {
            $title = 'Заказать проект';
        }

        return $this->formFactory->create(
            OrderType::class,
            $this->order,
            [
                'action' => $this->urlGenerator->generate('orderCreate'),
                'label' => $title
            ]
        );
    }

}