<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    private AuthenticationUtils $authenticationUtils;

    public function __construct(AuthenticationUtils $authenticationUtils)
    {
        $this->authenticationUtils = $authenticationUtils;
    }

    public function login(): Response
    {
        return $this->render('security/login.html.twig', [
            // last username entered by the user (if any)
            'last_email' => $this->authenticationUtils->getLastUsername(),
            // last authentication error (if any)
            'error' => $this->authenticationUtils->getLastAuthenticationError(),
        ]);
    }

    public function logout()
    {
        throw new \Exception('This should never be reached!');
    }
}
