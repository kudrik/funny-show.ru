<?php

declare(strict_types=1);

namespace App\Controller;

use App\Application\Breadcrumbs\Breadcrumbs;
use App\Application\Imageable\ImageableUrlGenerator;
use App\Application\Pagination\Pagination;
use App\Entity\Catalog;
use App\Entity\MetaInfo;
use App\Entity\Page;
use App\Service\OrderFormFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CatalogController extends AbstractController
{
    private ImageableUrlGenerator $imageableUrlGenerator;

    private OrderFormFactory $orderFormFactory;

    public function __construct(ImageableUrlGenerator $imageableUrlGenerator, OrderFormFactory $orderFormFactory)
    {
        $this->imageableUrlGenerator = $imageableUrlGenerator;
        $this->orderFormFactory = $orderFormFactory;
    }

    public function index(Request $request): Response
    {
        /** @var Page $page */
        $page = $this->getDoctrine()->getRepository(Page::class)->findOneBy(['parent' => null, 'url' => $request->get('_route')]);

        $meta = $page->meta->createWithDefaultTitle($page->getName())->createWithDefault(
            $this->getParameter('default_meta')
        );

        $catalogRepository = $this->getDoctrine()->getRepository(Catalog::class);

        $currentPageNumber = $request->get('page');

        $items = $catalogRepository->findBy(['published' => true], ['position' => 'asc']);

        /*
        $pagination = new Pagination(
            $catalogPager->getNbResults(),
            $catalogPager->getCurrentPage(),
            $catalogPager->getMaxPerPage(),
            'page'.$page->getId(),
            $filter
        );

        $itemsLinkParameters = $filter;
        $itemsLinkParameters['page'] = $catalogPager->getCurrentPage() > 1 ? $catalogPager->getCurrentPage() : null;
        */

        return $this->render(
            'catalog/index.html.twig',
            [
                'page' => $page,
                'items' => $items,
                //'itemsLinkParameters' => $itemsLinkParameters,
                'meta' => $meta,
                //'pagination' => $pagination,
            ]
        );

    }

    public function item(int $id, Request $request): Response
    {
        $catalogRepository = $this->getDoctrine()->getRepository(Catalog::class);

        /** @var Catalog $catalog */
        $catalog = $catalogRepository->find($id);

        if ($catalog && $catalog->published) {

            $this->orderFormFactory->setCatalog($catalog);

            $page = $this->getDoctrine()->getRepository(Page::class)->findCatalog();

            $meta = $catalog->getMeta()
                ->createWithDefault($this->getParameter('default_meta'))
                ->createWithCanonical($this->generateUrl('catalog', ['id' => $catalog->getId()], UrlGeneratorInterface::ABSOLUTE_URL));
            if ($catalog->getImageFileName()) {
                $meta = $meta->createWithImage($this->imageableUrlGenerator->getAssetPath($catalog,'large'));
            }

            $breadcrumbs = new Breadcrumbs();

            $breadcrumbs->add(
                $page->menu ? $page->menu->getFullName() : $page->getName(),
                $this->generateUrl(
                    'page'.$page->getId(),
                    [
                       //@TODO add filter parameters
                    ]
                )
            );

            $pagePhoto = $this->getDoctrine()->getRepository(Page::class)->findPhoto();

            return $this->render('catalog/item.html.twig', [
                'page' => $page,
                'catalog' => $catalog,
                'pagePhoto' => $pagePhoto,
                'breadcrumbs' => $breadcrumbs->getItems(),
                'meta' => $meta
            ]);
        }

        throw new NotFoundHttpException('Catalog item not found');
    }
}