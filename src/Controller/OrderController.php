<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Order;
use App\Entity\RequestInfo;
use App\Service\OrderFormFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends AbstractController
{
    private OrderFormFactory $formFactory;

    public function __construct(OrderFormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    public function create(Request $request): Response
    {
        $form = $this->formFactory->create();

        $form->handleRequest($request);

        /** @var Order $order */
        $order = $form->getData();

        $order->setRequestInfo(new RequestInfo($request->getClientIp(), $request->headers->get('referer')));

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($order);
            $this->getDoctrine()->getManager()->flush();

            return $this->render('order/_done.html.twig', ['order' => $order]);
        }

        return $this->render('order/_form.html.twig', ['orderForm' => $form->createView()]);
    }
}