<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\MetaInfo;
use App\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PageController extends AbstractController
{
    public function index(int $pageId): Response
    {
        $pageRepository =  $this->getDoctrine()->getRepository(Page::class);

        /** @var Page $page */
        $page = $pageRepository->find($pageId);

        if (!$page) {
            throw new NotFoundHttpException('Page not found');
        }

        $meta = $page->meta->createWithDefaultTitle($page->getName())->createWithDefault(
            $this->getParameter('default_meta')
        );

        $templateParameters = ['page' => $page, 'meta' => $meta];




        return $this->render('page/index.html.twig', $templateParameters);
    }
}