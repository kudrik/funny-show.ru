<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Catalog;
use App\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    public function index(): Response
    {
        $pageRepository = $this->getDoctrine()->getRepository(Page::class);

        $pages = $pageRepository->findBy(['parent' => null], ['position' => 'asc']);

        $catalogRepository = $this->getDoctrine()->getRepository(Catalog::class);

        $catalog = $catalogRepository->findForMainPage();

        return $this->render(
            'page/main.html.twig',
            ['pages' => $pages, 'catalog' => $catalog]
        );
    }
}