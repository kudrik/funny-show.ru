<?php

declare(strict_types=1);

namespace App\Controller;

use App\Application\Breadcrumbs\Breadcrumbs;
use App\Application\Imageable\ImageableUrlGenerator;
use App\Entity\MetaInfo;
use App\Entity\Page;
use App\Entity\Shop;
use App\Repository\PageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ShopController extends AbstractController
{
    private ImageableUrlGenerator $imageableUrlGenerator;

    public function __construct(ImageableUrlGenerator $imageableUrlGenerator)
    {
        $this->imageableUrlGenerator = $imageableUrlGenerator;
    }

    public function item(int $id): Response
    {
        $shopRepository = $this->getDoctrine()->getRepository(Shop::class);

        /** @var Shop|null $shop */
        $shop = $shopRepository->find($id);

        if (!$shop || !$shop->published) {
            throw new NotFoundHttpException('Shop item not found');
        }

        /** @var Page $page */
        $page = $shop->getPage();

        return $this->render(
            'shop/item.html.twig',
            [
                'page' => $page,
                'shop' => $shop,
                'breadcrumbs' => $this->buildBreadcrumbs($page, $shop)->getItems(),
                'meta' => $this->buildMeta($page, $shop),
                'rubricator' => $this->buildRubricator(),
            ]
        );
    }

    private function buildBreadcrumbs(Page $page, Shop $shop): Breadcrumbs
    {
        $breadcrumbs = new Breadcrumbs();

        /** @var PageRepository $pageRepository */
        $pageRepository = $this->getDoctrine()->getRepository(Page::class);

        foreach ($pageRepository->findAncestors($page) as $ancestor) {
            $breadcrumbs->add(
                $ancestor->menu ? $ancestor->menu->getFullName() : $ancestor->getName(),
                $this->generateUrl('page'.$ancestor->getId(), [])
            );
        }

        $breadcrumbs->add(
            $page->menu ? $page->menu->getFullName() : $page->getName(),
            $this->generateUrl('page'.$page->getId(), [])
        );

        $breadcrumbs->add($shop->name, null);

        return $breadcrumbs;
    }

    private function buildMeta(Page $page, Shop $shop): MetaInfo
    {
        $meta = $shop->getMeta()
            ->createWithDefault($this->getParameter('default_meta'))
            ->createWithCanonical(
                $this->generateUrl('shop'.$page->getId(), ['id' => $shop->getId()], UrlGeneratorInterface::ABSOLUTE_URL)
            );
        if ($shop->getImageFileName()) {
            $meta = $meta->createWithImage($this->imageableUrlGenerator->getAssetPath($shop, 'large'));
        }

        return $meta;
    }

    private function buildRubricator(): iterable
    {
        /** @var PageRepository $pageRepository */
        $pageRepository = $this->getDoctrine()->getRepository(Page::class);

        return $pageRepository->findShopRubricator();
    }
}